import React, {Component} from 'react';
import './App.css';
import './components/Button/Button.css';
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

class App extends Component {
    state = {
        firstModal: false,
        secondModal: false,
    };

    render () {
        const {firstModal, secondModal} = this.state;
        const funcOpen = this.openModal.bind(this);
        const funcClose = this.closeModal.bind(this);
        const actions = <>
            <button className='modal-btn' onClick={(e)=>this.closeModal(e)}>OK</button>
            <button className='modal-btn' onClick={(e)=>this.closeModal(e)}>Cancel</button></>;
        const modalBodyText = <>
                <p className="modal-body-text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                </p>
                <p className="modal-body-text">
                    Lorem ipsum dolor sit.
                </p>
            </>;
        const closeModalBtn = <span onClick={(e)=>this.closeModal(e)}>&times;</span>;

        return (
        <div className="App">
            <Button text='Open first modal' color='red-btn' funcOpen={funcOpen} value='firstModal'/>
            <Button text='Open second modal' color='green-btn' funcOpen={funcOpen} value='secondModal'/>
            {firstModal
                && <Modal actions={actions}
                     header='Do you want to delete this file?'
                     closeButton={closeModalBtn}
                     text={modalBodyText}
                          funcClose={funcClose}
            />}
            {secondModal
                && <Modal actions={actions}
                      header='Another text for Header'
                      closeButton={closeModalBtn}
                      text={modalBodyText}
                      styleMod='filtered'
                          funcClose={funcClose}

            />}
        </div>
      );
    }

    openModal(value) {
        this.setState({[value]: !this.state[value]})
    }

    closeModal(e) {

        this.setState({firstModal: false});
        this.setState({secondModal: false});
    }
  }

export default App;
