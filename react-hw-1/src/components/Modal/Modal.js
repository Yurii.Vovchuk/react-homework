import React, {Component} from 'react';
import './Modal.css'

class Modal extends Component {
state = {
    isButtonX: true,
};

    render() {
        const {actions, header, closeButton, text, styleMod} = this.props;
        const {isButtonX} = this.state;

        return (
            <div className='container' onClick={(event)=>this.bgCloseModal(event)}>
                <div className={`modal-body ${styleMod}`}>
                    <div className='modal-header'>
                        {header}
                        {isButtonX && closeButton}
                    </div>
                    <div className="modal-body-text">
                        {text}
                    </div>
                    {actions}
                </div>
            </div>
        );
    }
    bgCloseModal(event){
        if (event.target === event.currentTarget) {
            this.props.funcClose(this.props.value);
        }
    }
}

export default Modal;