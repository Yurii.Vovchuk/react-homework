import React, {Component} from 'react';
import './Button.css';

class Button extends Component {

    render() {
        const {text, color, funcOpen, value} = this.props;

        return (
                <button className={`button ${color}`}  onClick={()=> funcOpen(value)} >
                    {text}
                </button>
            );
    }
}

export default Button;